# micro:bitish Examples
 ## Purpose

This repository will contain corresponding examples of MicroPython code for various BBC micro:bit form factor boards.

---

## Boards 

> - [BBC micro:bit](https://microbit.org/new-microbit
>   )
> - [Adafruit CLUE](https://www.adafruit.com/clue)
> - [Banana Pi BPI:Bit](https://www.banana-pi.org/en/banana-pi-steam/63.html)
> - [Block:bit](https://www.aliexpress.com/item/1005002778526854.html?spm=a2g0o.order_list.0.0.6b9e1802nZngLz)
> - [BrainPad Pulse](https://www.brainpad.com/pulse/)
> - [Elecfreaks Pico:ed](https://www.elecfreaks.com/picoed.html)
> - [HiiBot BlueFi](https://python4bluefi.readthedocs.io/zh_CN/latest/)
> - [KittenBot Future Board](https://www.kittenbot.cc/products/future-board-esp32-aiot-python-education-kit)
> - [mPython 掌控板 (Master Board)](https://mpython.readthedocs.io/en/master/board/hardware.html)
> - [The BubbleWorks PicoBit](https://www.tindie.com/products/thebubbleworks/picobit/)

---

### BBC micro:bit

![BBB micro:bit](images/board-microbitv2.png)

---

### Adafruit CLUE

![Adafruit Clue](images/board-clue.png)

---

### Banana Pi BPI:Bit

![Banana Pi BPI:Bit](images/board-bpibit.png)

---

### Block:bit

![Block:bit](images/board-blockbit.png)

---

### BrainPad Pulse

![BrainPad Pulse](images/board-brainpad.png)

---

### Elecfreaks Pico:ed

![Elecfreaks Pico:ed](images/board-picoed.png)

---

### HiiBot BlueFi

![HiiBot BlueFi](images/board-bluefi.png)

---

### KittenBot Future Board

![KittenBot Future Board](images/board-futureboard.png)

---

### mPython 掌控板 (Master Board)

![mPython 掌控板](images/board-masterboard.png)



---

### The BubbleWorks PicoBit

![The BubbleWorks PicoBit](images/board-picobit.png)

---

## Comparison of Specifications

|                           | BBC micro:bit V2   | Adafruit CLUE | Banana Pi BPI:Bit | BrainPad Pulse | Elecfreaks Pico:ed | HiiBot BlueFi | KittenBot Future Board | mPython 掌控板 |
| ------------------------- | ------------------ | ------------- | ----------------- | -------------- | ------------------ | ------------- | ---------------------- | -------------- |
| Display                   | 5x5 red LED matrix |               |                   |                |                    |               |                        |                |
| Programmable<br />Buttons |                    |               |                   |                |                    |               |                        |                |
| Buzzer                    | ✓                  | ✓             | ✓                 | ✓              | ✓                  | ✓             | ✓                      | ✓              |
|                           |                    |               |                   |                |                    |               |                        |                |

